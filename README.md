# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Example of using NodeJS for Google Cloud Prediction API 

### Summary of set up ###
The only thing that you need to have outside the project it's an environment variable to point to the credentials file "credentials.json". You need to add the environment variable with name GOOGLE_APPLICATION_CREDENTIALS, and as value put the absolute path to the credentials.json file located on your system.

### Configuration ###
* Make sure to fill the credentials.json file with your own data, otherwise googleapis library won't find who you are, or which is your project.
* Run npm install in the root of the project where you cloned the repo. And after that you're all set up.

### Dependencies ###
* NodeJS
* googleapis library

### How to run ###
To run, just use the next command: 

* node app.js "good job"